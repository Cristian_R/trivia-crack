Games = new Mongo.Collection('games');

UsersIndex = new EasySearch.Index({
    collection: Meteor.users,
    fields: ['username'],
    engine: new EasySearch.Minimongo()
});

Collections = {};

var imageStore = new FS.Store.GridFS("images");

Collections.Images = new FS.Collection("images", {
    stores: [imageStore]
});
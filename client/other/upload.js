Template.form.events({
    'submit form': function (event) {
        event.preventDefault();
        var imageFile = event.currentTarget.children[0].children[0].children[0].files[0];


        if (imageFile == undefined) {
            Materialize.toast('Empty', 4000) // 4000 is the duration of the toast
        }



        Collections.Images.insert(imageFile, function (error, fileObject) {
            if (error) {
                alert("You are a fail, Cristian");
            } else {
                $('.grid').masonry('reloadItems');
            }


        });
    }
});

"use strict";

Template.post.helpers({
    postList: function () {
        return Collections.Images.find({});
    },

    updateMasonry: function () {
        $('.grid').imagesLoaded().done(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                percentPosition: true
                    //gutter:18;
            });
        });
    }
});